## Osiris OS
Hey folks, this is a project i was doing during my high school studies, while approaching at basics of x86Assembly I decided to go a bit deeper then the basic registry operations and build a os kernel. Since the idea as you could imagine it's awesomely crazy I've just realized after some days of studying and investigations that the thing was almost impossible. So I ended up building my small 16 bit compatible kernel routine and it's own bootloader, working on a FAT12 file system.

### Introduction

Osiris Os is a basic operating system composed by a bootloader and a very small kernel file, using 16 bit FAT file system. The bootloader routine load the kernel.bin file into the ram memory, using FAT16 file system. The system is emulated on VirtualBox machine because the CPU registers are used in real mode and not in protected mode, such as all the modern operating systems. The kernel routine implements a basic routine for printing blue background and a simple string printed on the screen. To building the os image, you must use nasm for assembly asm files, than mkdosfs for creating floppy drive image as FAT16 file system

### License

This software is released accordingly to the [WTFPL License](http://www.wtfpl.net/)

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)
