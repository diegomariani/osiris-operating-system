; ==================================================================
; OSIRIS Operating System - 2014 Diego Mariani
; Sistema operativo a 16 bit Real Mode. Usa FAT12 File system
; ==================================================================

	BITS 16

	jmp short originStart
	nop

; ------------------------------------------------------------------
; Disk description table
; Valori usati per il tipo di disco floppy 1.44 Mb
; ------------------------------------------------------------------

OEMLabel		db "ERISBOOT"	; Etichetta del disco
BytesPerSector		dw 512		; Numero di byte per settore di disco
SectorsPerCluster	db 1		; Numero di settori per ogni cluster
ReservedForBoot		dw 1		; Numero di settori riservati al boot
NumberOfFats		db 2		; Numero di FAT presenti nel fileSystem
RootDirEntries		dw 224		; Numero di entry della rootDir 224 ognuna di 32 bytes = 7168 bytes / 512 = 14 settori rootDir
LogicalSectors		dw 2880		; Numero di settori logici
MediumByte		db 0F0h		; Medium descriptor byte
SectorsPerFat		dw 9		; Numero di settori per FAT
SectorsPerTrack		dw 18		; Numero di settori per traccia
Sides			dw 2		; Numero di testine/drive
DriveNo			dw 0		; Numero del drive
Signature		db 41		; Numero del drive 41 per floppy
VolumeID		dd 00000000h	; Volume ID: qualsiasi numero
VolumeLabel		db "MIKEOS     ";Etichetta del floppy
FileSystem		db "FAT12   "	;Tipo di file system


;------------ INIZIO DEL BOOTLOADER -------------------
;Il primo MB di memoria dopo l'accensione si presenta come: CS:IP = F000h:FFF0h
;che corrisponde alla entry point del BIOS.Una volta terminata la procedura di setup del
;BIOS viene eseguito l'interrupt INT19H (interrupt di bootstrap).Questo interrupt
;carica il primo settore di disco 512byte in memoria RAM all'indirizzo 07C0h
;------------------------------------------------------

originStart:

mov ax,07C0h
add ax,544

;-----------------------------------------------
;Impostiamo 8k di memoria per il codice di boot
;Impostiamo 4k di memoria per lo stack
;-----------------------------------------------

cli
mov ss,ax
mov sp,4096
sti

;----------------------------------------------
;Impostiamo il data segment all'indirizzo 07C0h
;----------------------------------------------

mov ax,07C0h
mov ds,ax




;----------------------------------------------
;Per prima cosa bisogna caricare la root directory nella quale
;cercare il nome del file da caricare in memoria. Per capire l'inizio della rootDir:
;RootDirStart = reservedForBoot + N° Fats * SectorForFats = 1 + 18 = 19 settori logici
;NumeroRootDirEntries = ( rootDirEntries * 32 byte ) / 512 byte = 7168 / 512 = 14 settori logici
;dataStart = RootDirStart + NumeroRootDirEntries = 19 + 14 = 33 settore logico
;----------------------------------------------

;-----------------------------------------
;Cominciamo a leggere i blocchi (in ax il settore da leggere)
;-----------------------------------------
					; Ready to read first block of data
	mov ax, 19			; La rootDirectory comincia al settore 19
	call logicalToHeadTrackSector

	mov si, buffer			; Set ES:BX to point to our buffer (see end of code)
	mov bx, ds
	mov es, bx
	mov bx, si

	mov ah, 2			; Params for int 13h: read floppy sectors
	mov al, 14			; And read 14 of them

	pusha				; Prepare to enter loop

;-------------------------------------------
;Una volta letti i settori la root directory viene caricata in ES:BX quindi si trova in [buffer]
;-------------------------------------------

read_root_dir:
	popa				; In case registers are altered by int 13h
	pusha

	stc				; A few BIOSes do not set properly on error
	int 13h				; Read sectors using BIOS

	jnc search_dir			; If read went OK, skip ahead
	call reset_floppy		; Otherwise, reset floppy controller and try again
	jnc read_root_dir		; Floppy reset OK?

	jmp reboot			; If not, fatal double error


search_dir:
	popa

	mov ax, ds			; Root dir is now in [buffer]
	mov es, ax			; Set DI to this info
	mov di, buffer

	mov cx, word [RootDirEntries]	; Search all (224) entries
	mov ax, 0			; Searching at offset 0


next_root_entry:
	xchg cx, dx			; We use CX in the inner loop...

	mov si, kern_filename		; Start searching for kernel filename
	mov cx, 11
	rep cmpsb
	je found_file_to_load		; Pointer DI will be at offset 11

	add ax, 32			; Bump searched entries by 1 (32 bytes per entry)

	mov di, buffer			; Point to next entry
	add di, ax

	xchg dx, cx			; Get the original CX back
	loop next_root_entry

	mov si, file_not_found		; If kernel is not found, bail out
	call printf
	jmp reboot


;----------------------------------------
;Bisogna caricare la FAT nella ram e il primo cluster del file è il 26
;di si trova a 11 quindi : 11 + 15 (0fh) = 26
;----------------------------------------

found_file_to_load:			; Fetch cluster and load FAT into RAM
	mov ax, word [es:di+0Fh]	; Offset 11 + 15 = 26, contains 1st cluster
	mov word [cluster], ax

	mov ax, 1			; Sector 1 = first sector of first FAT
	call logicalToHeadTrackSector

	mov di, buffer			; ES:BX points to our buffer
	mov bx, di

	mov ah, 2			; int 13h params: read (FAT) sectors
	mov al, 9			; All 9 sectors of 1st FAT

	pusha				; Prepare to enter loop


read_fat:
	popa				; In case registers are altered by int 13h
	pusha

	stc
	int 13h				; Read sectors using the BIOS

	jnc read_fat_ok			; If read went OK, skip ahead
	call reset_floppy		; Otherwise, reset floppy controller and try again
	jnc read_fat			; Floppy reset OK?

; ******************************************************************
fatal_disk_error:
; ******************************************************************
	mov si, disk_error		; If not, print error message and reboot
	call printf
	jmp reboot			; Fatal double error


read_fat_ok:
	popa

	mov ax, 2000h			; Segment where we'll load the kernel
	mov es, ax
	mov bx, 0

	mov ah, 2			; int 13h floppy read params
	mov al, 1

	push ax				; Save in case we (or int calls) lose it


;----------------------------------------
;Per caricare il file e sapere a quale settore cominciare ClusterStart = [cluster] + userData = [cluster] + 31
;-----------------------------------------

load_file_sector:
	mov ax, word [cluster]		; Convert sector to logical
	add ax, 31

	call logicalToHeadTrackSector	; Make appropriate params for int 13h

	mov ax, 2000h			; Set buffer past what we've already read
	mov es, ax
	mov bx, word [pointer]

	pop ax				; Save in case we (or int calls) lose it
	push ax

	stc
	int 13h

	jnc calculate_next_cluster	; If there's no error...

	call reset_floppy		; Otherwise, reset floppy and retry
	jmp load_file_sector


	; In the FAT, cluster values are stored in 12 bits, so we have to
	; do a bit of maths to work out whether we're dealing with a byte
	; and 4 bits of the next byte -- or the last 4 bits of one byte
	; and then the subsequent byte!

calculate_next_cluster:
	mov ax, [cluster]
	mov dx, 0
	mov bx, 3
	mul bx
	mov bx, 2
	div bx				; DX = [cluster] mod 2
	mov si, buffer
	add si, ax			; AX = word in FAT for the 12 bit entry
	mov ax, word [ds:si]

	or dx, dx			; If DX = 0 [cluster] is even; if DX = 1 then it's odd

	jz even				; If [cluster] is even, drop last 4 bits of word
					; with next cluster; if odd, drop first 4 bits

odd:
	shr ax, 4			; Shift out first 4 bits (they belong to another entry)
	jmp short next_cluster_cont


even:
	and ax, 0FFFh			; Mask out final 4 bits


next_cluster_cont:
	mov word [cluster], ax		; Store cluster

	cmp ax, 0FF8h			; FF8h = end of file marker in FAT12
	jae end

	add word [pointer], 512		; Increase buffer pointer 1 sector length
	jmp load_file_sector


end:					; We've got the file to load!
	pop ax				; Clean up the stack (AX was pushed earlier)
	mov dl, byte [bootdev]		; Provide kernel with boot device info

	jmp 2000h:0000h			; Jump to entry point of loaded kernel!

;------------- Definizione delle funzioni base ------------------

;-----------------------------------------------------
;printf: stampa di una stringa posta in si finche non è 0
;-----------------------------------------------------

printf:
	pusha 	;salva tutti i registri nello stack
	mov ah,0Eh
.repeat:
	lodsb
	cmp al,0
	je .done
	int 10h
	jmp short .repeat
.done:
	popa 	;reimoosta i valori dei registri
ret


;-----------------------------------------------------
;logicalToHeadTrackSector: questa funzione converte settore logico in AX in Head Track e Sector per leggere dal device
;-----------------------------------------------------

logicalToHeadTrackSector:
	push bx
	push ax

	mov bx, ax			; sposto 19 in bx

	mov dx, 0
	div word [SectorsPerTrack]
	add dl, 01h
	mov cl, dl
	mov ax, bx

	mov dx, 0			; Calcolo la head (testina)
	div word [SectorsPerTrack]
	mov dx, 0
	div word [Sides]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]
ret


reset_floppy:
	push ax
	push dx
	mov ax, 0
	mov dl, byte [bootdev]
	stc
	int 13h
	pop dx
	pop ax
	ret

reboot:
	mov ax, 0
	int 16h				; in attesa di pressione di un tasto
	mov ax, 0
	int 19h

; ------------------------------------------------------------------
; VARIABILI

	kern_filename	db "KERNEL  BIN"	; OsirisOs kernel filename

	disk_error	db "Floppy error! Press any key...", 0
	file_not_found	db "KERNEL.BIN not found!", 0

	bootdev		db 0 	; Boot device number
	cluster		dw 0 	; Cluster del file che vogliamo caricare
	pointer		dw 0 	; puntantore al buffer per il caricamento del kernel


; ------------------------------------------------------------------
; FINE SETTORE DI BOOT E INIZIO DEL BUFFER

	times 510-($-$$) db 0	; Pad remainder of boot sector with zeros
	dw 0AA55h		; Boot signature la fine del settore di boot deve essere settata a questo valore


buffer:				; inizio dell'area di buffer


; ==================================================================

