BITS 16
jmp os_main

asd db 'Ciao Mondo !',0
errorDriverString db 'Errore durante inizializzazione dei drivers !',0
command db 'smart-console>',0
systemName db 'Osiris OS by Diego',0
input			times 256 db 0
	commandd			times 32 db 0

;-----------------------------------------------------
;printf: stampa di una stringa posta in si finche non è 0
;-----------------------------------------------------

printf:
	pusha 	;salva tutti i registri nello stack
	mov ah,0Eh
.repeat:
	lodsb
	cmp al,0
	je .done
	int 10h
	jmp short .repeat
.done:
	popa 	;reimoosta i valori dei registri
ret


;-----------------------------------------------------
;waitKey: attende in ax stringa
;-----------------------------------------------------

waitKey:
pusha

	mov ax, 0
	mov ah, 10h			; BIOS call to wait for key
	int 16h

	mov [.tmp_buf], ax		; Store resulting keypress

	popa				; But restore all other regs
	mov ax, [.tmp_buf]
	ret


	.tmp_buf	dw 0


;-----------------------------------------------------
;moveCursor: questa funzione sposta il cursore alla posizione 
;determinata da DH (riga) e DL (colonna)
;-----------------------------------------------------

moveCursor:
	pusha
	
	mov bh,0
	mov ah,2
	int 10h
	
	popa
ret


;-----------------------------------------------------
;hideCursor: questa funzione nasconde il cursore testo
;-----------------------------------------------------

hideCursor:
	pusha
	
	mov ch,32
	mov ah,1
	mov al,3
	int 10h
	
	popa
ret


;----------------------------------------------------
;renderBackground: questa funzione imposta il colore di sfondo 
;oltre a definire due barre top/bottom colorate con del testo al loro interno
;in input riceve in AX/BX top/bottom string e in cx il colore
;----------------------------------------------------

renderBackground:
	pusha

	mov dl,0	;sposto il cursore alla prima riga/colonna
	mov dh,0
	call moveCursor

	mov ah,09h	;interrupt per la stampa carattere con attributo cx volte
	mov bh,0
	mov cx,80
	mov bl,01110000b
	mov al,' '
	int 10h

	mov dh,1	;sposto il cursore alla seconda riga
	mov dl,0
	call moveCursor	

	mov ah,09h	;colore del resto della pagina
	mov bl,00011111b
	mov cx,1840
	mov bh,0
	mov al,' '
	int 10h

	mov dh,24
	mov dl,0
	call moveCursor

	mov ah,09h	;stampa ultima linea
	mov bh,0
	mov bl,01110000b
	mov cx,80
	mov al,' '
	int 10h
		
	mov dh, 24
	mov dl, 1
	call moveCursor
	mov si, systemName
	call printf

	mov dh, 0
	mov dl, 1
	call moveCursor
	mov si, systemName
	call printf

	popa
ret


;-----------------------------------------------------
;resetDrivers: questa funzione resetta i drivers e ne verifica lo status
;-----------------------------------------------------

resetDrivers:
	pusha
	
	mov ax,0h
	int 33h
	
	cmp ax,0h
	je driverError

	popa
ret

driverError:
	pusha
	
	mov si,errorDriverString
	call printf
	popa
ret


;-----------------------------------------------------
;showMousePointer: questa funzione visualizza il cursore del mouse a video
;-----------------------------------------------------

showMousePointer:
	pusha
	
	call resetDrivers
	mov ax,1h
	int 33h
	
	popa
ret

os_main:
	cli				; Clear interrupts
	mov ax, 0
	mov ss, ax			; Set stack segment and pointer
	mov sp, 0FFFFh
	sti				; Restore interrupts

	cld				; The default direction for string operations
					; will be 'up' - incrementing address in RAM

	mov ax, 2000h			; Set all segments to match where kernel is loaded
	mov ds, ax			
	mov es, ax			
	mov fs, ax			
	mov gs, ax

	mov si,asd

oop:	call renderBackground

	mov dh,2
	mov dl,0
	call moveCursor

	mov si,command
	call printf

