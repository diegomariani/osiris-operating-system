#!/bin/sh

# This script assembles the Osiris OS bootloader and kernel

# be sure that is root
if test "`whoami`" != "root" ; then
	echo "You must be logged in as root to build (for loopback mounting)"
	echo "Enter 'su' or 'sudo bash' to switch to root"
	exit
fi

# if not exists create new floppy image
if [ ! -e disk_img/osiris_os.flp ]
then
	echo ">>> Create osiris_os floppy image..."
	mkdosfs -C disk_img/osiris_os.flp 1440 || exit
fi

# with nasm assembly bootload.asm
echo ">>> Assembling bootloader..."

nasm -O0 -w+orphan-labels -f bin -o src/bootload/bootload.bin src/bootload/bootload.asm || exit

# with nasm assembly kernel.asm
echo ">>> Assembling Osiris OS kernel..."

cd src
nasm -O0 -w+orphan-labels -f bin -o kernel.bin kernel.asm || exit
cd ..

# adding bootloader.bin to first sector of the floppy disk image
echo ">>> Adding bootloader to floppy image..."

dd status=noxfer conv=notrunc if=src/bootload/bootload.bin of=disk_img/osiris_os.flp || exit

# copying kernel.bin into floppy image
echo ">>> Copy Osiris OS kernel..."

rm -rf tmp-loop

mkdir tmp-loop && mount -o loop -t vfat disk_img/osiris_os.flp tmp-loop && cp src/kernel.bin tmp-loop/

sleep 0.2

echo ">>> Unmounting loopback floppy..."

umount tmp-loop || exit

rm -rf tmp-loop

# create also iso image for other uses
echo ">>> Creating CD-ROM ISO image..."

rm -f disk_img/osiris_os.iso
mkisofs -quiet -V 'OSIRIS' -input-charset iso8859-1 -o disk_img/osiris_os.iso -b osiris_os.flp disk_img/ || exit

echo '>>> Completed Successfully !'
